.. _global-attributes:

Information on Entire Data Set (global attributes)
==================================================

The global attributes store descriptive information pertaining to the
entire dataset.

A distinction is made between :ref:`mandatory attributes <global-mandatory>` and
optional attributes (Chapter 3.2). The optional attributes are necessary
to accommodate the different data sources/disciplines.

*Attribute names* can generally be freely chosen, but there are
*attribute names* that are already reserved and can only be used for
specified purposes. These are as follows::

    add_offset, ancillary_variables, axis, bounds, calendar, cell_measures,
    cell_methods, climatology, comment, compress, Conventions, coordinates,
    _FillValue, flag_masks, flag_meanings, flag_values, formula_terms,
    grid_mapping, history, institution, leap_month, leap_year, long_name,
    missing_value, month_lengths, positive, references, scale_factor,
    source, standard_error_multiplier, standard_name, title, units,
    valid_max, valid_min, valid_range.

For the meaning of the attribute names listed above, please see Appendix
A in *netCDF Climate and Forecast (CF) Metadata Conventions* [7]_.


.. _global-mandatory:

Mandatory Global Attributes
---------------------------

*Global attributes* are the netCDF file’s metadata. Only information
that applies to the file as a whole is to be included here.

.. metadata_table:: data/global_mandatory.yml

.. [7] http://cfconventions.org/cf-conventions/cf-conventions.html#attribute-appendix


.. _global-optional:

Optional Global Attributes
--------------------------

Optional global attributes are not relevant at the Hereon for all netCDF
files, but agreements can be made as necessary in individual groups. If
the described attribute is relevant, then this attribute should be used
consistently.

The comment attribute should be used in particular to describe the file
contents.

The carrier attribute describes the carrier on which a platform is
installed temporarily—for example, RV Prandtl.

In COSYNA the platform attribute is mandatory and is usually specified.
An example of platform would be *"Ferrybox on RV Polarstern"*.

The origin of a netCDF file is described in lineage. This is usually an
original file in another format.

The individual sequence of processing should be described in
processingSteps.

StartTime and StopTime denote the first and last time values and
facilitate quick and easy access to these values—for example via
OPeNDAP.

Should the file be disseminated externally or published, the user rights
and obligations are outlined in the distribution_statement. The content
should be coordinated within the respective institute division.

In history one can directly record what was done to the file to reach
its current state. Tools like the CDOs or NCOs document the workflow in
the history automatically. One can also refer to an external file or URL
here.

PI should designate the scientist responsible for the entire project to
which the file belongs.

The following attributes are available for describing geographical
location: geospatial_lon_min/_max, geospatial_lat_min/_max or Bbox. They
are, for example, necessary for metadata catalogue inquiries.

.. metadata_table:: data/global_optional.yml

.. _global-dimensions:

Dimensions
----------

The data is stored as multi-dimensional fields in netCDF files. Decisive
structural elements are the *dimensions.* They are used to define the
internal coordinate structure of the data sets (*variables*). They
contain information on the spatial dimensions (nx,ny,nz) as well as the
time dimension (time). These are explained under *variables* (see
Section 4).

-  A *variable* can have any number of *dimensions*.

-  All *dimensions* must have different *names*.

-  A restriction to four *dimensions* is recommended.

-  Dimensions of a variable should be arranged in the relative order of
   date/time, height/depth, latitude, longitude (T, Z, Y, X). If this is
   not possible, (e.g., for spectra), other reasonable sequences should
   be created.

Additional *dimensions* must be to the left of (T,Z,Y,X).

If time series are stored in netCDF files, then one must keep in mind
that the series can be continued by adding new times. The time variable
dimension should be "UNLIMITED" to ensure expansion of the time series
without difficulties. It is then referred to as the *record dimension.*
